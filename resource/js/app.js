// app
;(jQuery(function($){
	$('[name="btn-print"]').on('click', function(){
		window.print();
	});
	$('[name="btn-view-answer"]').on('click', function(){
		$('#box-board table').addClass('end');
	});
	$('[name="btn-hide-answer"]').on('click', function(){
		$('#box-board table').removeClass('end');
	});
	$('[name="btn-generate"]').on('click', function(){
		let 단어들 = $('#words').val().toUpperCase().replace(/\s/g,'');
		let 한글사용 = 단어들.match(/[^0-9a-zA-Z, ]/);
		let 숫자사용 = 단어들.match(/[0-9]/);
		let 영어사용 = 단어들.match(/[a-zA-Z]/);
		단어들 = 단어들.split(',').map(val => $.trim(val));
		// 단어길이 큰것부터 정렬
		단어들.sort(function(a,b){
			return a.length > b.length ? -1 : (a.length < b.length ? 1 : 0);
		})
		let max_length = 단어들[0].length*1;
		max_length = 단어들.length > max_length ? 단어들.length : max_length;
		const 한줄갯수 = max_length + 2;

		// 박스 객체 생성
		let box = [];
		for(x=0; x<한줄갯수; x++) {
			box[x] = [];
			for(y=0; y<한줄갯수; y++) {
				box[x][y] = '';
			}
		}

		// 단어 채우기
		for( i in 단어들) {
			let 단어 = 단어들[i];
			if(i==0) { // 첫번째 글자는 아무곳에나 넣는다.
				box = 아무곳에나넣기(단어, 한줄갯수, box);
			} else { // 나머지 단어는
				box = 찾아서넣기(단어, 한줄갯수, box);
			}
		}
		let answer = clone(box);

		// 빈자리 채우기.
		for(x=0; x<한줄갯수; x++) {
			for(y=0; y<한줄갯수; y++) {
				box[x][y] = box[x][y].length>0 ? box[x][y] : rand_word(1, 한글사용, 숫자사용, 영어사용);
			}
		}

		// 박스 DOM 생성
		html =['<table>'];
		for(x=0; x<한줄갯수; x++) {
			html.push('<tr>');
			for(y=0; y<한줄갯수; y++) {
				html.push('<td '+(answer[x][y].length>0 ? 'class="answer"' : '')+'>'+(box[x]&&box[x][y] ? box[x][y] : '')+'</td>');
			}
			html.push('</tr>');
		}
		html.push('</table>');
		$('#box-board').html(html.join(''))
	});

	const clone = function(obj)  {
		return JSON.parse(JSON.stringify(obj))
	}
	const rand_name = function() {
		let text = "";
		let first = "김이박최정강조윤장임한오서신권황안송류전홍고문양손배조백허유남심노정하곽성차주우구신임나전민유진지엄채원천방공강현함변염양변여추노도소신석선설마주연방위표명기반왕모장남탁국여진구";
		let last = "가강건경고관광구규근기길나남노누다단달담대덕도동두라래로루리마만명무문미민바박백범별병보사산상새서석선설섭성세소솔수숙순숭슬승시신아안애엄여연영예오옥완요용우원월위유윤율으은의이익인일자잔장재전정제조종주준중지진찬창채천철초춘충치탐태택판하한해혁현형혜호홍화환회효훈휘희운모배부림봉혼황량린을비솜공면탁온디항후려균묵송욱휴언들견추걸삼열웅분변양출타흥겸곤번식란더손술반빈실직악람권복심헌엽학개평늘랑향울련";

		for (var i = 0; i < 1; i++)
		  text += first.charAt(Math.floor(Math.random() * first.length));
		for (var i = 0; i < 2; i++)
		  text += last.charAt(Math.floor(Math.random() * last.length));

		return text;
	}
	const rand_word = function(cnt, add_hangul, add_number, add_alphabet) {
		cnt = cnt>0 ? cnt : 1;
		let text = "";
		let word = '';
		if(add_hangul) {word += "가강건경고관광구규근기길나남노누다단달담대덕도동두라래로루리마만명무문미민바박백범별병보사산상새서석선설섭성세소솔수숙순숭슬승시신아안애엄여연영예오옥완요용우원월위유윤율으은의이익인일자잔장재전정제조종주준중지진찬창채천철초춘충치탐태택판하한해혁현형혜호홍화환회효훈휘희운모배부림봉혼황량린을비솜공면탁온디항후려균묵송욱휴언들견추걸삼열웅분변양출타흥겸곤번식란더손술반빈실직악람권복심헌엽학개평늘랑향울련";}
		if(add_number) {word += '123456789';}
		if(add_alphabet) {word += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';}
		for (var i = 0; i < cnt; i++)
		  text += word.charAt(Math.floor(Math.random() * word.length));

		return text;
	}
	const 아무곳에나넣기 = function(단어, 한줄갯수, box) {
		let x시작위치 = Math.round(Math.random() * 1000 % (한줄갯수-1))*1;
		let y시작위치 = Math.round(Math.random() * 1000 % 2)*1;
		let y끝 = 단어.length*1 + y시작위치;
		let 방향 = Math.random() < 0.5 ? 'x' : 'y';
		if(방향=='x') {
			for(l=y시작위치; l<y끝; l++) {
				box[x시작위치][l] = 단어.charAt(l-y시작위치);
			}
		} else {
			for(l=y시작위치; l<y끝; l++) {
				box[l][x시작위치] = 단어.charAt(l-y시작위치);
			}
		}
		return box;
	}
	const 찾아서넣기 = function(단어, 한줄갯수, b) {
		box = clone(b);

		// 단어중 글자가 있는 곳을 찾자 ... 겹쳐서 넣을려고 했는데 있는 글자를 찾은 다음 x방향 혹은 y방향으로 자리가 남아있는지 확인해야 함. .. 귀찮아 그냥 빈곳에 아무곳에나 넣자.
		// for(l=0; l<단어.length; l++) {
		// 	let 글자 = 단어.charAt(l);
		// 	for(x=0; x<한줄갯수; x++) {
		// 		for(y=0; y<한줄갯수; x++) {
		// 			if(box[x][y] == 글자) {
		// 				x시작위치 = x
		// 				y시작위치 = y
		// 			}
		// 		}
		// 	}
		// }

		한줄갯수 = 한줄갯수 * 1;
		let x시작위치 = Math.round(Math.random() * 1000 % (한줄갯수 - 단어.length*1))*1;
		let y시작위치 = Math.round(Math.random() * 1000 % (한줄갯수 - 단어.length*1))*1;
		let x끝 = 단어.length*1 + x시작위치;
		let y끝 = 단어.length*1 + y시작위치;

		// x 혹은 y 방향으로 글자를 넣을 수 있는지 보자.
		let 방향 = Math.random() < 0.5 ? 'x' : 'y';
		let 벼있나 = true;
		if(방향=='x') {
			for(x=x시작위치; x<x끝; x++) {
				if(box[x] && box[x][y시작위치] && box[x][y시작위치].length>0) {
					벼있나 = false;
					break;
				}
			}
			if(벼있나) {
				for(x=x시작위치; x<x끝; x++) {
					box[x] = box[x] ? box[x] : [];
					box[x][y시작위치] = 단어.charAt(x-x시작위치);
				}
			}
		} else {
			for(y=y시작위치; y<y끝; y++) {
				if(box[x시작위치] && box[x시작위치][y] && box[x시작위치][y].length>0) {
					벼있나 = false;
					break;
				}
			}
			if(벼있나) {
				// let y끝 = y시작위치*1 + 단어.length;
				for(y=y시작위치; y<y끝; y++) {
					box[x시작위치] = box[x시작위치] ? box[x시작위치] : [];
					box[x시작위치][y] = 단어.charAt(y-y시작위치);
				}
			}
		}
		if(!벼있나) {
			return 찾아서넣기(단어, 한줄갯수, box);
		} else {
			return box;
		}
	}
}))