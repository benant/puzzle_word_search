// i18n.js
;(function() {
    const support_lang = ['ko','en'],
		default_lang = 'ko';
    var lang_data = {},
        lang = navigator.language || navigator.userLanguage,
        cookielang = getCookie('lang');
    lang = lang.substr(0, 2);
    // lang = in_array(lang, support_lang) ? lang : default_lang; // 브라우저 언어 설정값을 기준으로 첫번째 언어를 선택하도록 할때
    lang = default_lang; // 브라우저 언어 설정에 상관없이 처음 언어 지정할때 사용.
	lang = cookielang && cookielang !== lang && in_array(cookielang, support_lang) ? cookielang : lang;

    if (cookielang !== lang) {
        setCookie('lang', lang, 365);
	}
	if (window.lang !== lang) {
		window.lang = lang;
	}

    const get_lang_data = function(callback) {
		let cache_time = Math.ceil(((new Date().getTime())/1000)/(60*60*1));
        let data_file = '/i18n/' + lang + '/LC_MESSAGES/WWW.json?v='+cache_time;
        httpRequest = new XMLHttpRequest();
        if (httpRequest) {
            httpRequest.onreadystatechange = function() {
                if (httpRequest.readyState === XMLHttpRequest.DONE) {
                    if (httpRequest.status === 200) {
                        r = JSON.parse(httpRequest.responseText);
						lang_data = r.data;
                    } else {
                        console.error(__('번역 데이터 가져오지 못함.'));
					}
					if(callback) {callback();}
                }
            };
            httpRequest.open('GET', data_file);
            httpRequest.send();
        }
	}
	get_lang_data();
	window.__ = function(key) {
		return lang_data && lang_data[key] ? lang_data[key] : key;
	};
	window._e = function(key) {
		document.write(__(key));
	};
	window._c = function(l, callback) {
		if(!in_array(l, support_lang)) {l=default_lang;}
		if(l!=lang) {
			lang = l;
			setCookie('lang', l, 365);
			get_lang_data(callback);
			// window.location.reload();
		}
	}

})();
